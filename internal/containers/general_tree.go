package containers

import "fmt"

// GeneralTreeInterface 通用树接口
type GeneralTreeInterface interface {
	SetLimitOfWith(limitOfWith int) *GeneralTreeImp
	AddNode(nodeData interface{}, mode ModeEnum) *GeneralTreeImp
	Print(mode ModeEnum)
	Depth() int
}

// 通用树-数据结构
/**************************
     	    N
	N       N      N    ...
  N N N   N N N  N N N  ...
*/
type generalTree struct {
	nodeData interface{}
	nodes    []*generalTree
}

// ModeEnum 插入节点模式
type ModeEnum uint8

const (
	// ModeDfs 深度优先插入模式
	ModeDfs ModeEnum = 1
	// ModeBfs 广度优先插入模式
	ModeBfs ModeEnum = 2
)

// GeneralTreeImp 通用树实现
type GeneralTreeImp struct {
	// 树
	tree *generalTree
	// 子节点最大限制 0 无限
	limitOfWith int
	// 节点总数
	totalOfNode int
}

// NewGeneralTree 新建一棵树
func NewGeneralTree() *GeneralTreeImp {
	return &GeneralTreeImp{tree: new(generalTree), limitOfWith: 0}
}

// SetLimitOfWith 设置最大子节点数量
func (gt *GeneralTreeImp) SetLimitOfWith(limitOfWith int) *GeneralTreeImp {
	if limitOfWith < 0 {
		panic("limitOfWith must be >= 0 ")
	}
	gt.limitOfWith = limitOfWith
	return gt
}

// AddNode 添加新节点
func (gt *GeneralTreeImp) AddNode(nodeData interface{}, mode ModeEnum) *GeneralTreeImp {
	var node = new(generalTree)
	node.nodeData = nodeData
	if mode == ModeDfs {
		gt.dfsInsert(gt.tree, node)
	}
	if mode == ModeBfs {
		gt.bfsInsert(gt.tree, node)
	}
	gt.totalOfNode++
	return gt
}

// Depth 获取树深度（最大高度）
func (gt *GeneralTreeImp) Depth() int {
	cur := gt.tree
	var i int
	for len(cur.nodes) > 0 {
		i += 1
		cur = cur.nodes[0]
	}
	return i
}

// Width 获取树宽度 （最大宽度行）
func (gt *GeneralTreeImp) Width() (int, int) {
	return gt.maxWidth(gt.tree, 0, 0)
}

// NumOfNode 节点总数
func (gt *GeneralTreeImp) NumOfNode() int {
	return gt.totalOfNode
}

func (gt *GeneralTreeImp) maxWidth(current *generalTree, maxW int, lv int) (int, int) {
	currentWidth := len(current.nodes)
	if maxW < currentWidth {
		maxW = currentWidth
	}
	if currentWidth > 0 {
		subMaxW := 0
		for _, v := range current.nodes {
			subMaxW += len(v.nodes)
		}

		gt.maxWidth(current.nodes[0], subMaxW, lv+1)
	}
	return maxW, lv
}

// Print 遍历打印输出树
func (gt *GeneralTreeImp) Print(mode ModeEnum) {
	if len(gt.tree.nodes) == 0 {
		fmt.Println(gt.tree.nodeData)
	} else {
		if mode == ModeDfs {
			fmt.Println("\n DFS: ")
			gt.dfsQuery(gt.tree, gt.tree)
		}
		if mode == ModeBfs {
			fmt.Println("\n BFS: ")
			gt.bfsQuery(gt.tree)
		}
	}
}

func (gt *GeneralTreeImp) dfsInsert(rootCurrent *generalTree, node *generalTree) {
	subNum := len(rootCurrent.nodes)
	if subNum == 0 {
		rootCurrent.nodes = append(rootCurrent.nodes, node)
	}
	if subNum > 0 {
		gt.dfsInsert(rootCurrent.nodes[0], node)
	}
}

func (gt *GeneralTreeImp) bfsInsert(rootCurrent *generalTree, node *generalTree) bool {
	subNum := len(rootCurrent.nodes)
	if subNum < gt.limitOfWith || gt.limitOfWith == 0 {
		rootCurrent.nodes = append(rootCurrent.nodes, node)
		return true
	} else {
		for _, next := range rootCurrent.nodes {
			if len(next.nodes) < gt.limitOfWith || gt.limitOfWith == 0 {
				next.nodes = append(next.nodes, node)
				return true
			}
		}
		gt.bfsInsert(rootCurrent.nodes[0], node)
		return true
	}
}

func (gt *GeneralTreeImp) dfsQuery(rootCurrent *generalTree, preNode *generalTree) {
	for _, next := range rootCurrent.nodes {
		gt.dfsQuery(next, rootCurrent)
	}
	if rootCurrent.nodeData != nil {
		fmt.Println(preNode.nodeData, " -> ", rootCurrent.nodeData)
	}
}

func (gt *GeneralTreeImp) bfsQuery(rootCurrent *generalTree) {
	for _, n := range rootCurrent.nodes {
		fmt.Println(rootCurrent.nodeData, " -> ", n.nodeData)
	}
	for _, next := range rootCurrent.nodes {
		gt.bfsQuery(next)
	}
}
