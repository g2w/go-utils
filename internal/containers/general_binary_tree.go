package containers

// 通用 二叉树 结构
type binaryTreeNode struct {
	data  interface{}     // 数据域
	left  *binaryTreeNode // 左节点
	right *binaryTreeNode // 右节点
}

// InsertPosition 插入位置 左子树｜右子树
type InsertPosition int

const (
	InsertLeft  InsertPosition = 1
	InsertRight InsertPosition = 2
)

// NewBinaryTreeNode 创建二叉树节点
func NewBinaryTreeNode(data interface{}) *binaryTreeNode {
	return &binaryTreeNode{data: data}
}

// Insert 插入节点
func (bt *binaryTreeNode) Insert(newNode *binaryTreeNode, ip InsertPosition) {
	bt.recInsert(newNode, bt, ip)
}

func (bt *binaryTreeNode) recInsert(newNode *binaryTreeNode, curr *binaryTreeNode, ip InsertPosition) {
	if curr.left == nil {
		curr.left = newNode
		return
	} else if curr.right == nil {
		curr.right = newNode
		return
	} else {
		if ip == InsertLeft {
			bt.recInsert(newNode, curr.left, ip)
		} else {
			bt.recInsert(newNode, curr.right, ip)
		}
	}
}

func (bt *binaryTreeNode) InsertLinked(newNode *binaryTreeNode, ip InsertPosition) *binaryTreeNode {
	var next *binaryTreeNode
	if bt.left == nil {
		bt.left = newNode
		next = bt
	} else if bt.right == nil {
		bt.right = newNode
		next = bt.left
	} else {
		if ip == InsertLeft {
			next = bt.left
		} else {
			next = bt.right
		}
	}
	return next
}
