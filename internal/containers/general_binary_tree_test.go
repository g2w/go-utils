package containers

import (
	"fmt"
	"testing"
)

func TestNewBinaryTreeNode(t *testing.T) {
	//tree := NewBinaryTreeNode(123)
	//tree.Insert(NewBinaryTreeNode(456), InsertLeft)
	//tree.Insert(NewBinaryTreeNode("789"), InsertLeft)
	//tree.Insert(NewBinaryTreeNode("abc"), InsertRight)
	//tree.Insert(NewBinaryTreeNode("def"), InsertRight)
	//tree.Insert(NewBinaryTreeNode("jkl"), InsertLeft)
	//tree.Insert(NewBinaryTreeNode("iop"), InsertLeft)
	//tree.Insert(NewBinaryTreeNode("666"), InsertRight)

	tree := NewBinaryTreeNode(123)

	tree.InsertLinked(NewBinaryTreeNode(456), InsertLeft).
		InsertLinked(NewBinaryTreeNode(789), InsertRight).
		InsertLinked(NewBinaryTreeNode("abc"), InsertRight).
		InsertLinked(NewBinaryTreeNode("def"), InsertRight)
	//InsertLinked(NewBinaryTreeNode("jkl"), InsertLeft).
	//InsertLinked(NewBinaryTreeNode("iop"), InsertLeft).
	//InsertLinked(NewBinaryTreeNode("666"), InsertRight)

	fmt.Println(tree)
}
