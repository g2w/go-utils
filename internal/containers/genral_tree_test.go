package containers

import (
	"fmt"
	"testing"
)

func TestNewGeneralTree(t *testing.T) {
	/*	tree := NewGeneralTree()
		tree.SetLimitOfWith(3).
			AddNode(123, ModeBfs).
			AddNode(456, ModeBfs).
			AddNode(789, ModeBfs).
			AddNode(222, ModeBfs).
			AddNode(333, ModeBfs).
			AddNode(444, ModeBfs).
			AddNode(555, ModeBfs)

		tree.Print(ModeBfs)
		tree.Print(ModeDfs)

		tree1 := NewGeneralTree()
		tree1.
			AddNode(123, ModeDfs).
			AddNode(456, ModeDfs).
			AddNode(789, ModeDfs).
			AddNode(222, ModeDfs).
			AddNode(333, ModeDfs).
			AddNode(444, ModeDfs).
			AddNode(555, ModeDfs)

		tree1.Print(ModeDfs)

		fmt.Println("Depth Tree: ", tree.Depth())
		fmt.Println("Depth Tree1: ", tree1.Depth())*/

	tree3 := NewGeneralTree()

	tree3.SetLimitOfWith(2).
		AddNode(1, ModeBfs).
		AddNode(2, ModeBfs).
		AddNode(5, ModeBfs).
		AddNode(51, ModeBfs).
		AddNode(52, ModeBfs).
		AddNode(6, ModeDfs).
		AddNode(7, ModeBfs).
		AddNode(9, ModeBfs).
		AddNode(10, ModeBfs)

	tree3.Print(ModeBfs)
	fmt.Println(tree3.Width())
	fmt.Println(tree3.Depth())
	fmt.Println(tree3.NumOfNode())

}
