package G2Log

import (
	"encoding/json"
	"fmt"
	"gitee.com/g2w/go-utils/pkg/G2File"
	"gitee.com/g2w/go-utils/pkg/G2Time"
	"io"
	"log"
	"os"
	"time"
)

type DataType map[string]interface{}

func ExceptionLog2File(filePath string, fileName string, logData DataType, Log2Console bool) {
	if !G2File.ExistPath(filePath) {
		_ = os.MkdirAll(filePath, os.ModePerm)
	}
	if !G2File.ExistsFile(fileName) {
		_ = G2File.CreateFile(fileName)
	}
	f, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		fmt.Println(err)
	}
	// 是否同时输出到控制台
	if Log2Console {
		log.SetOutput(io.MultiWriter(os.Stderr, f))
	} else {
		log.SetOutput(f)
	}
	// 屏蔽默认前缀时间
	log.SetFlags(0)
	// 开始时间
	startTime := time.Now()
	logData["start_time"] = G2Time.DateTimeF4ByNow(startTime)
	// 转成json字符串
	logFieldJson, _ := json.Marshal(logData)
	logFieldJsonString := string(logFieldJson)
	// 写日志行
	log.Println(logFieldJsonString)
}
