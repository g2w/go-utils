package G2File

import (
	"encoding/json"
	"os"
)

type DirInfo struct {
	FullPath   string
	Name       string
	IsDir      bool
	Size       int64
	Level      int
	ModeString string
	Subs       []*DirInfo
}

func GenDirJsonTree() string {
	pwd, _ := os.Getwd()
	files, _ := os.ReadDir(pwd)
	var tree = new(DirInfo)
	tree.IsDir = true
	tree.Size = 0
	tree.Level = 0
	tree.Name = "root"
	tree.FullPath = "/"
	for _, file := range files {
		fPath := pwd + "/" + file.Name()
		fInfo, _ := file.Info()
		nodeDir := &DirInfo{
			FullPath:   fPath,
			Name:       file.Name(),
			IsDir:      file.IsDir(),
			Size:       fInfo.Size(),
			Level:      1,
			ModeString: fInfo.Mode().String(),
			Subs:       []*DirInfo{},
		}
		tree.Subs = append(tree.Subs, nodeDir)
		if file.IsDir() {
			scanDir(fPath, nodeDir, 2)
		}
	}
	marshal, _ := json.Marshal(tree)
	return string(marshal)
}

func scanDir(pwd string, rootDir *DirInfo, lv int) {
	files, _ := os.ReadDir(pwd)
	for _, file := range files {
		fPath := pwd + "/" + file.Name()
		fInfo, _ := file.Info()
		nodeDir := &DirInfo{
			FullPath:   fPath,
			Name:       file.Name(),
			IsDir:      file.IsDir(),
			Size:       fInfo.Size(),
			Level:      lv,
			ModeString: fInfo.Mode().String(),
			Subs:       []*DirInfo{},
		}
		rootDir.Subs = append(rootDir.Subs, nodeDir)
		if file.IsDir() {
			scanDir(fPath, nodeDir, lv+1)
		}
	}
}
