package G2File

import (
	"fmt"
	"os"
)

// ExistsFile 判断文件是否存在
func ExistsFile(name string) bool {
	if _, err := os.Stat(name); err != nil {
		if os.IsNotExist(err) {
			return false
		}
	}
	return true
}

// ExistPath 目录是否存在
func ExistPath(path string) bool {
	_, err := os.Stat(path)
	if err != nil {
		if os.IsExist(err) {
			return true
		}
		if os.IsNotExist(err) {
			return false
		}
		fmt.Println(err)
		return false
	}
	return true
}

// CreateFile 创建文件
func CreateFile(name string) error {
	fo, err := os.Create(name)
	if err != nil {
		return err
	}
	defer func() {
		err = fo.Close()
		if err != nil {
			panic(err)
		}
	}()
	return nil
}

// CreatePathAll 创建目录
func CreatePathAll(path string, perm os.FileMode) error {
	return os.MkdirAll(path, perm)
}
