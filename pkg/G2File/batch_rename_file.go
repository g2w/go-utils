package G2File

import (
	"fmt"
	"io/fs"
	"os"
	"path/filepath"
)

func RenameFileBatch() error {
	var dir string = "./dir1"
	dir, _ = filepath.Abs(dir)
	type fInfo struct {
		fullPath string
		path     string
		fullName string
		name     string
		ext      string
	}
	files := make([]fInfo, 0)
	err1 := filepath.Walk(dir, func(path string, info fs.FileInfo, err error) error {
		if !info.IsDir() {
			fullName := info.Name()
			extName := filepath.Ext(path)
			files = append(files, fInfo{
				fullPath: path,
				path:     filepath.Dir(path),
				fullName: fullName,
				name:     fullName[0 : len(fullName)-len(extName)],
				ext:      extName,
			})
		}
		return nil
	})
	if err1 != nil {
		return err1
	}
	for _, f := range files {
		newFileName := filepath.Join(f.path, f.name)
		fmt.Println(newFileName)
		err2 := os.Rename(f.fullPath, newFileName)
		if err2 != nil {
			return err2
		}
	}
	return nil
}
