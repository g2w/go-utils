package G2Jwt

import (
	"github.com/dgrijalva/jwt-go"
	"time"
)

type Claims struct {
	Data interface{}
	jwt.StandardClaims
}

type Generator struct {
	jwtSecret []byte
	expiresAt int64
	issuer    string
	claims    Claims
}

// 指定加解密秘钥
func (g *Generator) withSecret(jwtSecret string) *Generator {
	g.jwtSecret = []byte(jwtSecret)
	return g
}

// 带上数据
func (g *Generator) withData(data interface{}) *Generator {
	g.claims.Data = data
	return g
}

// 带上签发人
func (g *Generator) withIssuer(issuer string) *Generator {
	g.issuer = issuer
	return g
}

// effectiveTime : 7 * 24 * 60 * 60 * time.Second （一周）
func (g *Generator) withEffectiveTime(effectiveTime time.Duration) *Generator {
	nowTime := time.Now()
	expireTime := nowTime.Add(effectiveTime)
	g.expiresAt = expireTime.Unix()
	return g
}

// GenerateToken 生成token
func (g *Generator) GenerateToken() (string, error) {
	tokenClaims := jwt.NewWithClaims(jwt.SigningMethodHS256, g.claims)
	//该方法内部生成签名字符串，再用于获取完整、已签名的token
	token, err := tokenClaims.SignedString(g.jwtSecret)
	return token, err
}

// ParseToken 解析token
func (g *Generator) ParseToken(token string) (interface{}, error) {
	tokenClaims, err := jwt.ParseWithClaims(
		token, &Claims{}, func(token *jwt.Token) (interface{}, error) {
			return g.jwtSecret, nil
		})
	if tokenClaims != nil {
		if claims, ok := tokenClaims.Claims.(*Claims); ok && tokenClaims.Valid {
			return claims, nil
		}
	}
	return nil, err
}
