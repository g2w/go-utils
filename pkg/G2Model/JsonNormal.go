package G2Model

import (
	"bytes"
	"database/sql/driver"
	"encoding/json"
	"errors"
)

/*
type DemoTable struct {
	Id 			int
  	JsonData 	JsonNormal  	`gorm:"type:json;comment:数据"`
}
*/

type JsonNormal []byte

func (j JsonNormal) Value() (driver.Value, error) {
	if j.IsNull() {
		return nil, nil
	}
	return string(j), nil
}
func (j *JsonNormal) Scan(input interface{}) error {
	if input == nil {
		*j = nil
		return nil
	}
	s, ok := input.([]byte)
	if !ok {
		return errors.New("json scan error")
	}
	*j = append((*j)[0:0], s...)
	return nil
}
func (j *JsonNormal) UnmarshalJSON(data []byte) error {
	if j == nil {
		return errors.New("marshal error")
	}
	*j = append((*j)[0:0], data...)
	return nil
}
func (j JsonNormal) MarshalJSON() ([]byte, error) {
	if j == nil {
		return []byte("null"), nil
	}
	return j, nil
}

func (j JsonNormal) Equals(j1 JsonNormal) bool {
	return bytes.Equal([]byte(j), []byte(j1))
}

func (j JsonNormal) IsNull() bool {
	return len(j) == 0 || string(j) == "null"
}

func ParseJson2Struct(jData JsonNormal, template interface{}) error {
	return json.Unmarshal(jData, &template)
}
