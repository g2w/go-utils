package G2Model

import (
	"database/sql/driver"
	"fmt"
	"time"
)

// TimeNormal 内嵌方式（推荐）
/*
type BaseModel struct {
	ID        uint
	CreatedAt TimeNormal
	UpdatedAt TimeNormal
}
*/
type TimeNormal struct {
	time.Time
}

// MarshalJSON 格式化时间
func (t TimeNormal) MarshalJSON() ([]byte, error) {
	tune := t.Format(`"2006-01-02 15:04:05"`)
	return []byte(tune), nil
}

// Value 解析自定义时间
func (t TimeNormal) Value() (driver.Value, error) {
	var zeroTime time.Time
	if t.Time.UnixNano() == zeroTime.UnixNano() {
		return nil, nil
	}
	return t.Time, nil
}
func (t *TimeNormal) Scan(v interface{}) error {
	value, ok := v.(time.Time)
	if ok {
		*t = TimeNormal{Time: value}
		return nil
	}
	return fmt.Errorf("can not convert %v to timestamp", v)
}
