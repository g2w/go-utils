
### GORM 日期时间字段 和 JSON字段 解析 说明


1、引入

```golang
import (
	"gitee.com/g2w/go-utils/G2Model"
)
```

2、模型定义

```golang

type BaseModel struct {
	ID        uint `gorm:"primarykey"`
	CreatedAt TimeNormal
	UpdatedAt TimeNormal
	DeletedAt gorm.DeletedAt
	Remark    string `gorm:"comment:备注"`
}


type Test struct {
	BaseModel
	JsonData Model.JsonNormal  `gorm:"type:json;not null;comment:数据"`
}
```

3、查询使用

```golang

    // 连接数据库查询
    var t models.Test
    database.DbConn.Model(&models.Test{}).First(&t)

    // json字段的模板数据结构定义
    type Company struct {
        Name        string      `json:"name"`
        Addr        string      `json:"addr"`
    }
    type Tpl struct {
        ID          int64       `json:"id"`
        Name        string      `json:"name"`
        Company     Company     `json:"company_info"`
    }
    var data Tpl
    //_ = G2Model.ParseJson2Struct(t.JsonData, &data)
    _ = json.Unmarshal(t.JsonData, &data)

    fmt.Println(data)

```

3、数据API响应

```golang
    func(context *gin.Context) {
        var t models.Test
        database.DbConn.Model(&models.Test{}).First(&t)
        // 输出会自动解析成json格式到前端
        context.JSON(200, t)
    }
```



